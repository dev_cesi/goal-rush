import { serve } from "@hono/node-server";
import { Hono } from "hono";
import { getAllMatches, getMatchById } from './controllers/matchController';

const app = new Hono();

app.get("/", async (c) => {
  return c.text("Hello, world!");
});

app.get('/matches', getAllMatches);
app.get('/matches/:id', getMatchById);

const port = 3000;
console.log(`Server is running on port ${port}`);

serve({
  fetch: app.fetch,
  port,
});
