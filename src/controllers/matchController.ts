// "Context" représente le contexte de la requête HTTP dans Hono (contient des informations sur la requête entrante)
import { Context } from 'hono';
import { MatchService } from '../services/matchService.service';

const matchService = new MatchService();

export const getAllMatches = async (c: Context) => {
    const matches = await matchService.getAllMatches();
    return c.json(matches);
};

export const getMatchById = async (c: Context) => {
    const id = parseInt(c.req.param('id'), 10);
    const match = await matchService.getMatchById(id);
    if (match) {
        return c.json(match);
    } else {
        return c.notFound();
    }
};