import prisma from '../../prisma/prisma';

export class MatchService {
    async getAllMatches() {
        return await prisma.match.findMany();
    }

    async getMatchById(id: number) {
        return await prisma.match.findUnique({
            where: { id },
        });
    }
}